Daserver site
----------------------------

[Prototype](https://design.penpot.app/#/view/103e00a0-a041-11ec-b568-c7b48f1fafc8?page-id=77823820-a16e-11ec-b11a-c19900929150&section=interactions&index=0&share-id=e9bb3a00-a244-11ec-a0b4-765163de8d2d)

# Commands
* `npm run dev` build develop environment static pages
* `npm run watch` watcher changing in js, css or html files
* `npm run start` start web server and watch to changing files
* `npm run build` build production environment static pages