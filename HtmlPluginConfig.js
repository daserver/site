const fs = require('fs');
const path = require("path");
const HtmlWebpackPlugin = require("html-webpack-plugin");

function getFiles(target) {
    target = path.resolve(__dirname, target);

    const dirents = fs.readdirSync(target, {
        withFileTypes: true
    });

    let listFiles = [];

    for (const item of dirents) {

        const filePath = path.resolve(target, item.name);

        if (item.isFile()) {
            if (path.extname(filePath) === '.html') {
                listFiles.push(filePath);
            }
        }

        if (item.isDirectory()) {

            listFiles = [
                ...listFiles,
                ...getFiles(filePath),
            ];

        }

    }
    return listFiles;
}

function convertPathes(pathList, prefix) {
    const result = [];

    for (const filePath of files) {
        let htmlPlugin =  new HtmlWebpackPlugin({
            template: filePath,
            filename: filePath.slice(prefix.length).replace(/^\//, ''),
            inject: false
        });
        result.push(htmlPlugin);

    }
    return result;
}

const mainDir = path.resolve(__dirname, 'src', 'html', 'views');
const files = getFiles(mainDir);
const objects = convertPathes(files, mainDir);


module.exports = objects;