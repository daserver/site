const { createSitemap } = require('sitemap');
const fs = require('fs');
const path = require("path");
const nodeFlags = require('node-flag')

function getFiles(target) {
    target = path.resolve(__dirname, target);

    const dirents = fs.readdirSync(target, {
        withFileTypes: true
    });

    let listFiles = [];

    for (const item of dirents) {

        const filePath = path.resolve(target, item.name);

        if (item.isFile()) {
            if (path.extname(filePath) === '.html') {
                listFiles.push(filePath);
            }
        }

        if (item.isDirectory()) {

            listFiles = [
                ...listFiles,
                ...getFiles(filePath),
            ];

        }

    }
    return listFiles;
}

function generateSiteMap(pathList, prefix) {
    const sitemap = createSitemap ({
        hostname: 'https://raiseddog.ru',
        cacheTime: 0
    });
    sitemap.add({url: '/', priority: 1.0, lastmodfile: 'src/html/views/index.html'});
    for (const filePath of files) {
        let sitemapUrl = filePath.slice(prefix.length).replace(/^\//, '').replace(/index\.html/g, '');
        if (sitemapUrl !== '') {
            sitemap.add({url: sitemapUrl, priority: 0.8, lastmodfile: filePath})
        }
    }
    sitemap.del('404.html');
    return sitemap;
}

const mainDir = path.resolve(__dirname, 'src', 'html', 'views');
const files = getFiles(mainDir);
const generatedSitemap = generateSiteMap(files, mainDir);
const prettiedSiteMap = generatedSitemap.toString(true);

if (nodeFlags.isset('output')) {
    console.log(prettiedSiteMap);
}

fs.writeFile('src/sitemap.xml', prettiedSiteMap, (err) => {
    if (err) throw err;
    console.log('The file has been saved!');
});