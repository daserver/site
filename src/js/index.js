document.addEventListener('DOMContentLoaded', function () {
    burgerMenuListener();
    document.querySelector('.current-year').innerHTML = (new Date).getFullYear();
});

function burgerMenuListener() {
    const burgerMenu = document.querySelector('#burgerMenu');
    const menu = document.querySelector('#menu');
    const burgerButton = burgerMenu.querySelector('.burger');

    burgerMenu.addEventListener('click', () => {
        if(menu.classList.contains('active')) {
            menu.classList.remove('active');
            burgerButton.classList.remove('active');
        } else {
            menu.classList.add('active');
            burgerButton.classList.add('active');
        }
    });
}